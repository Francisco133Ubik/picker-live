//
//  CameraView.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 03/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

protocol CameraDataOutputProtocol: AnyObject {
    func colorPixelOutput(_ color: UIColor)
    func presentAlertSettings()
}

final class CameraView: UIView {
    weak var delegate: CameraDataOutputProtocol?
    var previousColor = UIColor.black
    
    private lazy var videoDataOutput: AVCaptureVideoDataOutput = {
        let v = AVCaptureVideoDataOutput()
        v.alwaysDiscardsLateVideoFrames = true
        v.setSampleBufferDelegate(self, queue: videoDataOutputQueue)
        v.connection(with: .video)?.isEnabled = true
        return v
    }()

    private let videoDataOutputQueue: DispatchQueue = DispatchQueue(label: "JKVideoDataOutputQueue")
    private lazy var previewLayer: AVCaptureVideoPreviewLayer = {
        let l = AVCaptureVideoPreviewLayer(session: session)
        l.videoGravity = .resizeAspectFill
        return l
    }()
    
    private var captureDevice: AVCaptureDevice? = AVCaptureDevice.default(.builtInWideAngleCamera,
                                                                          for: .video,
                                                                          position: .back)
    private lazy var session: AVCaptureSession = {
        let s = AVCaptureSession()
        s.sessionPreset = .medium
        return s
    }()
    
    var positionCamera: AVCaptureDevice.Position = .back{didSet{
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            switchCamera(position: positionCamera)
        case .denied:
            delegate?.presentAlertSettings()
        default: return
        }
        }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        contentMode = .scaleAspectFit
        backgroundColor = .lightGray
    }
    
    public func startSession(){
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            beginSession()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    DispatchQueue.main.async {
                        self.beginSession()
                    }
                }
            }
        case .denied:
            delegate?.presentAlertSettings()
        default: return
        }
    }
    
   
    private func beginSession() {
        do {
            guard let captureDevice = captureDevice else { return }
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            if session.canAddInput(deviceInput) {
                session.addInput(deviceInput)
            }
            if session.canAddOutput(videoDataOutput) {
                session.addOutput(videoDataOutput)
            }
            layer.masksToBounds = true
            layer.addSublayer(previewLayer)
            previewLayer.frame = bounds
            session.sessionPreset = .high
            session.startRunning()
        } catch let error {
            debugPrint("\(self.self): \(#function) line: \(#line).  \(error.localizedDescription)")
        }
    }

    
    public func stopSession(){
        session.stopRunning()
    }
    
    public func resumeSession(){
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized: session.startRunning()
        case .denied: delegate?.presentAlertSettings()
        default: return
        }
    }
    
    public func switchCamera(){
        switch positionCamera {
        case .back: positionCamera = .front
        case .front: positionCamera = .back
        default: return
        }
    }
    
    public func switchCamera(position: AVCaptureDevice.Position){
        session.beginConfiguration()
        guard let currentCameraInput = session.inputs.first else {
            return
        }
        session.removeInput(currentCameraInput)
        captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: position)
        guard let captureDevice = captureDevice else { return }
        do{
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            if session.canAddInput(deviceInput) {
                session.addInput(deviceInput)
            }
            session.commitConfiguration()
        }catch let error{
            print(error)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        previewLayer.frame = bounds
    }
    
    func convert(cmage: CIImage) -> UIImage {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
}

extension CameraView: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {return}
        let ciimage = CIImage(cvPixelBuffer: imageBuffer)
        let image = convert(cmage: ciimage)
        let centerPoint = CGPoint(x: image.size.width / 2, y: image.size.height / 2)
        
        let currentPixer = image.getPixelColor(centerPoint)
        if UIColor.deltaRGB(color1: previousColor, color2: currentPixer) > 0.05 {
            previousColor = currentPixer
            delegate?.colorPixelOutput(currentPixer)
        }
    }
}
