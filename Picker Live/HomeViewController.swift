//
//  HomeViewController.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 01/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit
import AVFoundation
import WidgetKit

enum TypeSource{
    case camera
    case gallery
}

class HomeViewController: UIViewController {
    let cameraView = CameraView(frame: .zero)
    let indicatorColorView = IndicatorViewColor(frame: .zero)
    let pointCenterView = UIView(frame: .zero)
    var collectionView: UICollectionView!
    let shotButton = UICaptureButton(frame: .zero)
    let buttonMenu = UIButton(frame: .zero)
    let buttonHistory = UIButton(frame: .zero)
    let buttonCamera = UIButton(frame: .zero)
    var colors = [UIColor](){didSet{
        buttonHistory.isEnabled = !colors.isEmpty
        if colors.count == 10{
            AppStoreReviewManager.requestReviewIfAppropriate()
        }
        saveHistory()
        WidgetCenter.shared.reloadAllTimelines()
        }}
    var currentInsertAnimation = false
    var typeSource = TypeSource.camera
    let imageSelectView = UIImageView(frame: .zero)
    let backgroundImageSelectView = UIView(frame: .zero)
    var centerXAnchorConstraintPointCenter: NSLayoutConstraint!
    var centerYAnchorConstraintPointCenter: NSLayoutConstraint!
    var offSetCenter: CGPoint = .zero
    var realImageRect: CGRect?
    let shapeLayer = CAShapeLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .secondarySystemBackground
        setupView()
        setupCollectionView()
        setupGestures()
        fetchHistory()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupShapeAnimationLayer()
        cameraView.startSession()
    }
    
    private func saveHistory(){
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: colors, requiringSecureCoding: false) as Data
            UserDefaults(suiteName: SuiteName)?.set(data, forKey: ColorsKey)
            UserDefaults(suiteName: SuiteName)?.synchronize()
        } catch { }
    }
    
    private func fetchHistory(){
        guard let data = UserDefaults(suiteName: SuiteName)?.object(forKey: ColorsKey) as? Data else {
            return
        }
        do {
            guard let colors = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [UIColor]  else {
                return
            }
            self.colors = colors
        }catch {}
    }
    
    private func setupShapeAnimationLayer(){
        shapeLayer.frame = CGRect(x: view.frame.midX - CGFloat(90).dp,
                                  y: cameraView.frame.midY - CGFloat(90).dp,
                                  width: CGFloat(180).dp,
                                  height: CGFloat(180).dp)
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: CGFloat(180).dp, height: CGFloat(180).dp),
                                cornerRadius: CGFloat(90).dp)
        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 1
        view.layer.addSublayer(shapeLayer)
        layerScaleAnimation(layer: shapeLayer, duration: 0.85, fromValue: 1, toValue: 1.5)
    }
    
    func layerScaleAnimation(layer: CALayer, duration: CFTimeInterval, fromValue: CGFloat, toValue: CGFloat) {
        let scaleAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        CATransaction.begin()
        CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name: .easeInEaseOut))
        scaleAnimation.duration = duration
        scaleAnimation.fromValue = fromValue
        scaleAnimation.autoreverses = true
        scaleAnimation.toValue = toValue
        scaleAnimation.repeatCount = .infinity
        scaleAnimation.isRemovedOnCompletion = false
        layer.add(scaleAnimation, forKey: "scale")
        CATransaction.commit()
    }
    
    private func setupGestures(){
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizer(_:)))
        pointCenterView.addGestureRecognizer(panGesture)
    }
    
    private func setupView(){
        view.add(buttonMenu) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.setTitle("", for: .normal)
            $0.setImage(UIImage(named: "icon_menu"), for: .normal)
            $0.contentMode = .scaleAspectFit
            $0.imageEdgeInsets = UIEdgeInsets(top: CGFloat(14).dp, left: CGFloat(11).dp, bottom: CGFloat(14).dp, right: CGFloat(11).dp)
            NSLayoutConstraint.activate([
                $0.heightAnchor.constraint(equalToConstant: CGFloat(44).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(44).dp),
                $0.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: CGFloat(2).dp),
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: CGFloat(12).dp)
            ])
            $0.addTarget(self, action: #selector(tapButtonMenu), for: .touchUpInside)
        }
        view.add(cameraView) {
            $0.delegate = self
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.widthAnchor.constraint(equalTo: view.widthAnchor),
                $0.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.65),
                $0.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                $0.topAnchor.constraint(equalTo: buttonMenu.bottomAnchor, constant: CGFloat(8).dp)
            ])
        }
        view.add(backgroundImageSelectView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .clear
            NSLayoutConstraint.activate([
                $0.widthAnchor.constraint(equalTo: cameraView.widthAnchor),
                $0.heightAnchor.constraint(equalTo: cameraView.heightAnchor),
                $0.centerXAnchor.constraint(equalTo: cameraView.centerXAnchor),
                $0.centerYAnchor.constraint(equalTo: cameraView.centerYAnchor)
            ])
        }
        view.add(imageSelectView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .clear
            $0.contentMode = .scaleAspectFit
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: cameraView.topAnchor, constant: CGFloat(20).dp),
                $0.bottomAnchor.constraint(equalTo: cameraView.bottomAnchor, constant: -CGFloat(20).dp - 22),
                $0.leadingAnchor.constraint(equalTo: cameraView.leadingAnchor, constant: CGFloat(20).dp),
                $0.trailingAnchor.constraint(equalTo: cameraView.trailingAnchor, constant: -CGFloat(20).dp),
            ])
        }
        view.add(buttonHistory) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.setTitle("", for: .normal)
            $0.setImage(UIImage(named: "icon_history"), for: .normal)
            $0.imageEdgeInsets = UIEdgeInsets(top: CGFloat(11).dp, left: CGFloat(11).dp, bottom: CGFloat(11).dp, right: CGFloat(11).dp)
            $0.addTarget(self, action: #selector(presentHistoryVC), for: .touchUpInside)
            $0.isEnabled = false
            NSLayoutConstraint.activate([
                $0.heightAnchor.constraint(equalToConstant: CGFloat(44).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(44).dp),
                $0.centerYAnchor.constraint(equalTo: buttonMenu.centerYAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -CGFloat(12).dp)
            ])
        }
        view.add(pointCenterView) { pointView in
            pointView.translatesAutoresizingMaskIntoConstraints = false
            pointView.backgroundColor = .clear
            pointView.isUserInteractionEnabled = false
            centerXAnchorConstraintPointCenter = pointView.centerXAnchor.constraint(equalTo: cameraView.centerXAnchor)
            centerYAnchorConstraintPointCenter = pointView.centerYAnchor.constraint(equalTo: cameraView.centerYAnchor)
            NSLayoutConstraint.activate([
                centerYAnchorConstraintPointCenter,
                centerXAnchorConstraintPointCenter,
                pointView.heightAnchor.constraint(equalToConstant: 55),
                pointView.widthAnchor.constraint(equalToConstant: 55)
            ])
            pointView.add(UICircleView(frame: .zero)) {
                $0.translatesAutoresizingMaskIntoConstraints = false
                $0.backgroundColor = UIColor.white.withAlphaComponent(0.8)
                NSLayoutConstraint.activate([
                    $0.centerYAnchor.constraint(equalTo: pointView.centerYAnchor),
                    $0.centerXAnchor.constraint(equalTo: pointView.centerXAnchor),
                    $0.heightAnchor.constraint(equalToConstant: CGFloat(14).dp),
                    $0.widthAnchor.constraint(equalToConstant: CGFloat(14).dp)
                ])
            }
        }
        view.add(indicatorColorView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: pointCenterView.topAnchor, constant: 10),
                $0.centerXAnchor.constraint(equalTo: pointCenterView.centerXAnchor),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(50).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(140).dp)
            ])
        }
        
        view.add(UIView(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .secondarySystemBackground
            $0.clipsToBounds = true
            $0.layer.cornerRadius = 11
            $0.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            $0.dropShadow(shadowOffset: CGSize(width: 0, height: -2), radius: 4, opacity: 0.25)
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: cameraView.bottomAnchor, constant: -22),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
        }
        
        view.add(shotButton) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.heightAnchor.constraint(equalToConstant: CGFloat(75).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(75).dp),
                $0.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                $0.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -CGFloat(20).dp)
            ])
            $0.addTarget(self, action: #selector(tapCaptureButton), for: .touchUpInside)
            $0.dropShadowButton()
        }
        view.add(buttonCamera) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.setTitle("", for: .normal)
            $0.setImage(UIImage(named: "icon_camera"), for: .normal)
            $0.imageView?.contentMode = .scaleAspectFit
            $0.imageEdgeInsets = UIEdgeInsets(top: CGFloat(5).dp, left: CGFloat(5).dp, bottom: CGFloat(5).dp, right: CGFloat(5).dp)
            $0.addTarget(self, action: #selector(switchCamera), for: .touchUpInside)
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: shotButton.centerYAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -CGFloat(40).dp),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(44).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(44).dp)
            ])
        }
        view.add(UIButton(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.setTitle("", for: .normal)
            $0.setImage(UIImage(named: "icon_gallery"), for: .normal)
            $0.imageView?.contentMode = .scaleAspectFit
            $0.imageEdgeInsets = UIEdgeInsets(top: CGFloat(5).dp, left: CGFloat(5).dp, bottom: CGFloat(5).dp, right: CGFloat(5).dp)
            $0.addTarget(self, action: #selector(openGallery), for: .touchUpInside)
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: shotButton.centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: CGFloat(40).dp),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(44).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(44).dp)
            ])
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(deleteHistory),
                                               name: NSNotification.Name(rawValue: "deleteHistory"),
                                               object: nil)
    }
    
    @objc func deleteHistory(){
        colors.removeAll()
        collectionView.reloadData()
    }
    
    
    @objc func tapButtonMenu(){
        impactFeedback()
        let vc = MenuViewController()
        present(vc, animated: true, completion: nil)
    }
    
    
    @objc func panGestureRecognizer(_ sender: UIPanGestureRecognizer){
        switch sender.state {
        case .began:
            offSetCenter = CGPoint(x: centerXAnchorConstraintPointCenter.constant,
                                   y: centerYAnchorConstraintPointCenter.constant)
        case .changed:
            guard let rectImage = realImageRect else {return}
            let translation = sender.translation(in: view)
            offSetCenter.x += translation.x
            offSetCenter.y += translation.y
            if imageSelectView.convert(rectImage, to: nil).contains(CGPoint(x: cameraView.center.x + offSetCenter.x,
                                                                            y: cameraView.center.y + offSetCenter.y)){
                centerXAnchorConstraintPointCenter.constant = offSetCenter.x
                centerYAnchorConstraintPointCenter.constant = offSetCenter.y
                sender.setTranslation(.zero, in: view)
                setCenterColorImage()
            }
            view.layoutIfNeeded()
        default: return
        }
    }
    
    @objc func switchCamera(){
        impactFeedback()
        switch typeSource {
        case .camera:
            cameraView.switchCamera()
        case .gallery:
            typeSource = .camera
            backgroundImageSelectView.backgroundColor = .clear
            imageSelectView.image = nil
            cameraView.resumeSession()
            pointCenterView.isUserInteractionEnabled = false
            animateResetPointView()
            buttonCamera.setImage(UIImage(named: "icon_camera"), for: .normal)
        }
    }
    
    private func animateResetPointView(){
        UIView.animate(withDuration: 0.35, animations: {
            self.centerXAnchorConstraintPointCenter.constant = 0
            self.centerYAnchorConstraintPointCenter.constant = 0
            self.view.layoutIfNeeded()
        }) { _ in
            switch self.typeSource{
            case .gallery: self.setCenterColorImage()
            case .camera: self.shapeLayer.isHidden = false
            }
        }
    }
    
    private func setCenterColorImage(){
        let pointCenter = imageSelectView.convert(pointCenterView.center, from: nil)
        indicatorColorView.newColor = imageSelectView.colorOfPoint(point: pointCenter)
    }
    
    @objc func openGallery(){
        impactFeedback()
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func presentHistoryVC(){
        impactFeedback()
        let vc = HistoryColorsViewController()
        vc.colors = colors
        present(vc, animated: true, completion: nil)
    }
    
    private func setupImageSelected(image: UIImage){
        realImageRect = AVMakeRect(aspectRatio: image.size, insideRect: imageSelectView.bounds)
        backgroundImageSelectView.backgroundColor = image.averageColor
        imageSelectView.image = image
        cameraView.stopSession()
        typeSource = .gallery
        pointCenterView.isUserInteractionEnabled = true
        animateResetPointView()
        shapeLayer.isHidden = true
        buttonCamera.setImage(UIImage(named: "icon_option_camera"), for: .normal)
    }
        
    private func animationNewColor(info: InfoSnapshot, completion: @escaping() -> ()){
        let snapshot = indicatorColorView.snaphotColor()
        guard let image = snapshot.image else {return}
        let newFrame = indicatorColorView.convert(snapshot.frame, to: view)
        let snapshotImage = UIImageViewCircle(frame: newFrame)
        snapshotImage.image = image
        view.addSubview(snapshotImage)
        guard let attributes = collectionView.layoutAttributesForItem(at: IndexPath(item: 0, section: 0)) else {return}
        let cellFrameInSuperview = collectionView.convert(attributes.frame, to: view)
        let animation = UIViewPropertyAnimator(duration: 0.8, dampingRatio: 0.9) {
            snapshotImage.center = CGPoint(x: self.collectionView.frame.width / 2, y: cellFrameInSuperview.midY)
        }
        animation.addCompletion { (_) in
            snapshotImage.removeFromSuperview()
            completion()
        }
        animation.startAnimation()
    }
    
    private func setupCollectionView(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(ColorPickerCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        view.add(collectionView) {
            $0.dataSource = self
            $0.delegate = self
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.heightAnchor.constraint(equalToConstant: CGFloat(40).dp),
                $0.widthAnchor.constraint(equalTo: view.widthAnchor),
                $0.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                $0.topAnchor.constraint(equalTo: cameraView.bottomAnchor)
            ])
        }
    }
    
    @objc func tapCaptureButton(){
        view.isUserInteractionEnabled = false
        currentInsertAnimation = true
        colors.insert(indicatorColorView.getColor(), at: 0)
        collectionView.insertItems(at: [IndexPath(item: 0, section: 0)])
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: [.centeredVertically, .centeredHorizontally], animated: true)
        animationNewColor(info: indicatorColorView.snaphotColor()) {
            self.currentInsertAnimation = false
            self.collectionView.reloadItems(at: [IndexPath(item: 0, section: 0)])
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func impactFeedback(){
        guard Model.sharedModel.settings.isHapticsEnable else { return }
        let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .light)
        impactFeedbackgenerator.prepare()
        impactFeedbackgenerator.impactOccurred()
    }
}

extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imageSelected = info[.originalImage] as? UIImage else {
            picker.dismiss(animated: true, completion: nil)
            return
        }
        picker.dismiss(animated: true, completion: nil)
        setupImageSelected(image: imageSelected)
    }
}

extension HomeViewController: CameraDataOutputProtocol{
    func colorPixelOutput(_ color: UIColor) {
        DispatchQueue.main.async { self.indicatorColorView.newColor = color }
    }
    
    func presentAlertSettings() {
        let alertViewController = UIAlertController(title: "Find Color",
                                                    message: "Let Find Color access to camera to capture the colors of your environment live",
                                                    preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: "Accept", style: .default, handler: { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }))
        alertViewController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertViewController, animated: true, completion: nil)
    }
}
