//
//  OnboardingViewController.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 12/09/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    var collectionView: UICollectionView!
    let pageControl = UIPageControl(frame: .zero)
    let buttonSkip = UIButton(frame: .zero)
    let buttonNext = UIButton(frame: .zero)
    let model = OnboardingModel()
    var isCollectionSetup = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupView()
    }
    
    private func setupView(){
        guard !isCollectionSetup else {
            return
        }
        isCollectionSetup = true
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: layout)
        view.add(collectionView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.contentInsetAdjustmentBehavior = .never
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            $0.register(OnboardingCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
            $0.isPagingEnabled = true
            $0.showsHorizontalScrollIndicator = false
            $0.delegate = self
            $0.dataSource = self
            $0.bounces = false
        }
        
        view.add(pageControl) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.numberOfPages = model.pages.count
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -CGFloat(10).dp),
                $0.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            ])
            $0.pageIndicatorTintColor = .systemGray3
            $0.currentPageIndicatorTintColor = .systemBlue
        }
        view.add(buttonSkip) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.setTitle("Skip", for: .normal)
            $0.setTitleColor(.secondaryLabel, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(16).dp, weight: .medium)
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: pageControl.centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: CGFloat(24).dp)
            ])
            $0.addTarget(self, action: #selector(tapButtonSkip), for: .touchUpInside)
        }
        view.add(buttonNext) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.setTitle("Next", for: .normal)
            $0.setTitleColor(.systemBlue, for: .normal)
            $0.titleLabel?.font = UIFont.systemFont(ofSize: CGFloat(16).dp, weight: .bold)
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: pageControl.centerYAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -CGFloat(24).dp)
            ])
            $0.addTarget(self, action: #selector(tapButtonNext), for: .touchUpInside)
        }
    }
    
    private func presentHomeViewController(){
        navigationController?.pushViewController(HomeViewController(), animated: true)
    }
    
    
    @objc func tapButtonSkip(){
        presentHomeViewController()
    }
    
    @objc func tapButtonNext(){
        guard let centerIndexPath = collectionView.centerCellIndexPath else {
            return
        }
        if centerIndexPath.item != model.pages.count - 1{
            collectionView.scrollToItem(at: IndexPath(item: centerIndexPath.item + 1, section: 0),
                                        at: [.centeredVertically, .centeredHorizontally],
                                        animated: true)
        }else{
            presentHomeViewController()
        }
    }
    
}

extension OnboardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? OnboardingCollectionViewCell else {
            fatalError()
        }
        cell.page = model.pages[safe: indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if pageControl.currentPage == 2{
            buttonNext.setTitle("Started", for: .normal)
        }else{
            buttonNext.setTitle("Next", for: .normal)
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if pageControl.currentPage == 2{
            buttonNext.setTitle("Started", for: .normal)
        }else{
            buttonNext.setTitle("Next", for: .normal)
        }
    }
}


class OnboardingCollectionViewCell: UICollectionViewCell {
    let imageView = UIImageView(frame: .zero)
    let titleLabel = UILabel(frame: .zero)
    let subtitleLabel = UILabel(frame: .zero)
    let bottomView = UIView(frame: .zero)
    
    
    var page: OnboardingModel.Page?{didSet{
        guard let page = page else {
            return
        }
        titleLabel.text = page.title
        subtitleLabel.text = page.subtitle
        imageView.image = page.image
        }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        clipsToBounds = true
        add(bottomView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
                $0.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.35)
            ])
            $0.backgroundColor = .secondarySystemGroupedBackground
            $0.layer.cornerRadius = 20
            $0.clipsToBounds = true
        }
        add(imageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .black
            $0.contentMode = .scaleAspectFill
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.bottomAnchor.constraint(equalTo: bottomView.topAnchor, constant: 20)
            ])
        }
        bringSubviewToFront(bottomView)
        
        bottomView.add(subtitleLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.textColor = .secondaryLabel
            $0.font = UIFont.systemFont(ofSize: CGFloat(18).dp, weight: .regular)
            $0.textAlignment = .center
            $0.numberOfLines = 0
            NSLayoutConstraint.activate([
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(300).dp),
                $0.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor)
            ])
        }
        
        bottomView.add(titleLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.textColor = .label
            $0.font = UIFont.systemFont(ofSize: CGFloat(26).dp, weight: .medium)
            $0.textAlignment = .center
            $0.numberOfLines = 0
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.bottomAnchor.constraint(equalTo: subtitleLabel.topAnchor, constant: -CGFloat(20).dp)
            ])
        }

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        bottomView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}


struct OnboardingModel {
    let pages = [Page(title: "Live Capture",
                      subtitle: "Capture the colors of your environment live",
                      image: UIImage(named: "onboarding1")),
                 Page(title: "Capture from Images",
                      subtitle: "Select photos to determine the color at a specific point",
                      image: UIImage(named: "onboarding2")),
                 Page(title: "Save Colors",
                      subtitle: "Save captured colors and create beautiful color palettes",
                      image: UIImage(named: "onboarding3"))]
    
    
    struct Page {
        let title: String
        let subtitle: String
        let image: UIImage?
    }
    
}



