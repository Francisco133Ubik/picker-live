//
//  Model.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 22/08/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit
import StoreKit

let ColorsKey = "colors"
let SuiteName = "group.com.picker"

class Model {
    static let sharedModel = Model()
    var settings = Settings()
}


struct Settings: Codable {
    var appearances: TintApp = .system
    var isHapticsEnable: Bool = true
    
    enum TintApp: Int, Codable {
        case light
        case dark
        case system
    }
}


enum AppStoreReviewManager {
  static func requestReviewIfAppropriate() {
     SKStoreReviewController.requestReview()
  }
}
