//
//  HomeViewController+UICollectionDelegates.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 09/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ColorPickerCollectionViewCell else {
            fatalError()
        }
        if currentInsertAnimation && indexPath.row == 0{
            cell.viewBackgroundColor.alpha = 0
            cell.viewColor.alpha = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
           guard let customCell = cell as? ColorPickerCollectionViewCell else {return}
           customCell.colorIndicator = colors[safe: indexPath.item]
       }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.indexPathsForSelectedItems?.first {
        case .some(indexPath): return CGSize(width:  CGFloat(113).dp, height: CGFloat(36).dp)
        default: return CGSize(width:  CGFloat(36).dp, height: CGFloat(36).dp)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(15).dp
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        impactFeedback()
        collectionView.performBatchUpdates(nil, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: (collectionView.frame.width / 2) - (CGFloat(36).dp / 2), bottom: 0, right: CGFloat(20).dp)
    }
}
