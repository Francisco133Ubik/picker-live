//
//  UICollectionView+extension.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 12/09/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

extension UICollectionView {
    var centerPoint: CGPoint {
        get {
            return CGPoint(x: self.center.x + self.contentOffset.x,
                           y: self.center.y + self.contentOffset.y)
        }
    }
    var centerCellIndexPath: IndexPath? {
        if let centerIndexPath = self.indexPathForItem(at: self.centerPoint) {
            return centerIndexPath
        }
        return nil
    }
}
