//
//  CGFloat+extension.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 03/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

extension CGFloat{
    var dp: CGFloat{
        return (self / 375) * UIScreen.main.bounds.width
    }
}
