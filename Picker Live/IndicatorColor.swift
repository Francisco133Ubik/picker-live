//
//  IndicatorColor.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 03/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

typealias InfoSnapshot = (frame: CGRect, image: UIImage?)

class IndicatorViewColor: UIView {
    private var colorView: UIView = {
        let colorView = UIView()
        colorView.backgroundColor = .black
        colorView.clipsToBounds = true
        colorView.layer.masksToBounds = true
        return colorView
    }()
    
    private var colorLabel: UILabel = {
        let colorLabel = UILabel()
        colorLabel.textColor = .black
        colorLabel.textAlignment = .left
        colorLabel.text = "#000000"
        colorLabel.font = UIFont.systemFont(ofSize: CGFloat(14).dp, weight: .medium)
        return colorLabel
    }()
    
    public var newColor: UIColor = .black{didSet{
        colorView.backgroundColor = newColor
        colorLabel.textColor = newColor
        colorLabel.text = newColor.toHexString().uppercased()
        }}
    
    public func snaphotColor() -> InfoSnapshot{
        return (colorView.frame, colorView.snapshot())
    }
    
    public func getColor() -> UIColor{
        return colorView.backgroundColor ?? .black
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        backgroundColor = UIColor.clear
        isUserInteractionEnabled = false
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(blurView, at: 0)
        blurView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        blurView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        blurView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        blurView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        clipsToBounds = true
        layer.masksToBounds = true
        addSubview(colorView)
        colorView.translatesAutoresizingMaskIntoConstraints = false
        colorView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5).isActive = true
        colorView.widthAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5).isActive = true
        colorView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        colorView.leadingAnchor.constraint(equalToSystemSpacingAfter: leadingAnchor, multiplier: 1.8).isActive = true
        addSubview(colorLabel)
        colorLabel.translatesAutoresizingMaskIntoConstraints = false
        colorLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: colorView.trailingAnchor, multiplier: 1.4).isActive = true
        colorLabel.trailingAnchor.constraint(lessThanOrEqualToSystemSpacingAfter: trailingAnchor, multiplier: 1.1).isActive = true
        colorLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        colorView.layer.cornerRadius = colorView.frame.height / 2
    }
}
