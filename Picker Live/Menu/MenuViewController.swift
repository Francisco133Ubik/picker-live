//
//  MenuViewController.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 17/08/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    let headerView = UIView(frame: .zero)
    let tableView = UITableView(frame: .zero, style: .insetGrouped)
    let menuModel = Menu()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupTableView()
    }
    
    private func setupView(){
        view.backgroundColor = .secondarySystemBackground
    }
    
    private func setupTableView(){
        view.add(headerView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .secondarySystemGroupedBackground
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(50).dp)
            ])
        }
        headerView.add(UILabel(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.font = UIFont.systemFont(ofSize: CGFloat(14).dp, weight: .bold)
            $0.text = "Settings"
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: headerView.centerXAnchor)
            ])
        }
        headerView.add(UIButton(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .clear
            $0.setImage(UIImage(systemName: "multiply"), for: .normal)
            $0.tintColor = .secondaryLabel
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: CGFloat(12).dp),
                $0.heightAnchor.constraint(equalToConstant: 44),
                $0.widthAnchor.constraint(equalToConstant: 44)
            ])
            $0.addTarget(self, action: #selector(tapDismiss), for: .touchUpInside)
        }
        
        view.add(tableView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            $0.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            $0.delegate = self
            $0.dataSource = self
        }
    }
    
    @objc func tapDismiss(){
        dismiss(animated: true, completion: nil)
    }
    
    @objc func valueChangedSwitch(_ sender: UISwitch){
        Model.sharedModel.settings.isHapticsEnable = sender.isOn
        saveChanges()
    }
    
    private func saveChanges(){
        let appearances = Model.sharedModel.settings
        UserDefaults.standard.set(try? PropertyListEncoder().encode(appearances), forKey:"appearances")
    }
    
    
    func clearHistory(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "deleteHistory"), object: nil)
        let alertController = UIAlertController(title: "Clear History", message: "History deleted successfully", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Accept", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return menuModel.sectionTop.count
        case 1: return menuModel.sectionBottom.count
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        switch indexPath.section {
        case 0:
            guard let option = menuModel.sectionTop[safe: indexPath.item] else {
                return cell
            }
            cell.accessoryType = option.accesoryType
            cell.textLabel?.text = option.title
            cell.imageView?.image = option.iconImage
            if option.withStwich{
                let switchOption = UISwitch(frame: .zero)
                switchOption.tag = indexPath.item
                switchOption.addTarget(self, action: #selector(valueChangedSwitch(_:)), for: .valueChanged)
                if indexPath.item == 2 {
                    switchOption.setOn(Model.sharedModel.settings.isHapticsEnable, animated: false)
                }
                cell.accessoryView = switchOption
            }
            cell.tintColor = .secondaryLabel
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let option = menuModel.sectionBottom[safe: indexPath.item] else {
                return cell
            }
            cell.accessoryType = option.accesoryType
            cell.textLabel?.text = option.title
            cell.imageView?.image = option.iconImage
            cell.tintColor = .secondaryLabel
            cell.selectionStyle = .none
            return cell
        default: return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.item {
            case 0: present(AppTintViewController(), animated: true, completion: nil)
            case 1: present(AppIconsViewController(), animated: true, completion: nil)
            default: return
            }
        case 1: clearHistory()
        default: return
        }
    }
}


struct Menu {
    let sectionTop = [Option(title: "App Tint", iconImage: UIImage(systemName: "paintbrush")),
                      Option(title: "App Icon", iconImage: UIImage(systemName: "app")),
                      Option(title: "Haptics", iconImage: UIImage(systemName: "waveform.path"), accesoryType: .none, withStwich: true)]
    
    let sectionBottom = [Option(title: "Clear History", iconImage: UIImage(systemName: "trash"), accesoryType: .none )]
    
    
    struct Option {
        let title: String
        let iconImage: UIImage?
        var accesoryType: UITableViewCell.AccessoryType = .disclosureIndicator
        var withStwich: Bool = false
    }
}
