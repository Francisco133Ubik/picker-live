//
//  AppIconsViewController.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 06/09/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

class AppIconsViewController: UIViewController {
    var collectionView: UICollectionView!
    let headerView = UIView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        view.backgroundColor = .secondarySystemBackground
        view.add(headerView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .secondarySystemGroupedBackground
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(50).dp)
            ])
        }
        headerView.add(UILabel(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.font = UIFont.systemFont(ofSize: CGFloat(14).dp, weight: .bold)
            $0.text = "App Icons"
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: headerView.centerXAnchor)
            ])
        }
        headerView.add(UIButton(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .clear
            $0.setImage(UIImage(systemName: "multiply"), for: .normal)
            $0.tintColor = .secondaryLabel
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: CGFloat(12).dp),
                $0.heightAnchor.constraint(equalToConstant: 44),
                $0.widthAnchor.constraint(equalToConstant: 44)
            ])
            $0.addTarget(self, action: #selector(tapDismiss), for: .touchUpInside)
        }
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.add(collectionView) {
            $0.contentInset = UIEdgeInsets(top: CGFloat(22).dp, left: 0, bottom: 0, right: 0)
            $0.backgroundColor = .systemGroupedBackground
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            $0.register(IconCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
            $0.delegate = self
            $0.dataSource = self
        }
    }
    
    @objc func tapDismiss(){
        dismiss(animated: true, completion: nil)
    }
    
    
}


extension AppIconsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? IconCollectionViewCell else {
            fatalError()
        }
        cell.item = indexPath.item
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width * 0.9, height: CGFloat(60).dp)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard UIApplication.shared.supportsAlternateIcons  else {
            return
        }
        switch indexPath.item {
        case 0: UIApplication.shared.setAlternateIconName(nil)
        case 1: UIApplication.shared.setAlternateIconName("icon2")
        case 2: UIApplication.shared.setAlternateIconName("icon3")
        case 3: UIApplication.shared.setAlternateIconName("icon4")
        default: return
        }
    }
    
}


class IconCollectionViewCell: UICollectionViewCell {
    let iconImageView = UIImageView(frame: .zero)
    let titleLabel = UILabel(frame: .zero)
    
    var item: Int?{didSet{
        guard let item = item else {return}
        switch item {
        case 0:
            iconImageView.image = UIImage(named: "icon1")
            titleLabel.text = "Default"
        case 1:
            iconImageView.image = UIImage(named: "icon2")
            titleLabel.text = "Default - Dark"
        case 2:
            iconImageView.image = UIImage(named: "icon3")
            titleLabel.text = "Alternative"
        case 3:
            iconImageView.image = UIImage(named: "icon4")
            titleLabel.text = "Alternative - Dark"
        default: return
        }
        }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        backgroundColor = UIColor.secondarySystemGroupedBackground.withAlphaComponent(0.5)
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = CGFloat(4).dp
        add(iconImageView) {
            $0.clipsToBounds = true
            $0.layer.masksToBounds = true
            $0.layer.cornerRadius = CGFloat(4).dp
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: CGFloat(8).dp),
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(45).dp),
                $0.widthAnchor.constraint(equalToConstant: CGFloat(45).dp)
            ])
        }
        add(titleLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: CGFloat(12).dp)
            ])
        }
    }
    
}
