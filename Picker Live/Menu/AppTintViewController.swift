//
//  AppTintViewController.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 22/08/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

class AppTintViewController: UIViewController {
    let tableView = UITableView(frame: .zero, style: .insetGrouped)
    let headerView = UIView(frame: .zero)

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
   
    private func setupView(){
        view.backgroundColor = .secondarySystemBackground
        view.add(headerView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .secondarySystemGroupedBackground
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(50).dp)
            ])
        }
        headerView.add(UILabel(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.font = UIFont.systemFont(ofSize: CGFloat(14).dp, weight: .bold)
            $0.text = "App Tint"
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: headerView.centerXAnchor)
            ])
        }
        headerView.add(UIButton(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .clear
            $0.setImage(UIImage(systemName: "multiply"), for: .normal)
            $0.tintColor = .secondaryLabel
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: headerView.centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: CGFloat(12).dp),
                $0.heightAnchor.constraint(equalToConstant: 44),
                $0.widthAnchor.constraint(equalToConstant: 44)
            ])
            $0.addTarget(self, action: #selector(tapDismiss), for: .touchUpInside)
        }
        
        view.add(tableView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: headerView.bottomAnchor),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
            $0.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            $0.delegate = self
            $0.dataSource = self
        }
    }
    
    @objc func tapDismiss(){
        dismiss(animated: true, completion: nil)
    }

    @objc func valueChangeTint(sender: UISwitch){
        switch sender.tag {
        case 0:
            UIApplication.shared.connectedScenes.forEach { (scene: UIScene) in
                switch UIScreen.main.traitCollection.userInterfaceStyle{
                case .dark:
                    if sender.isOn{
                        Model.sharedModel.settings.appearances = .system
                        (scene.delegate as? SceneDelegate)?.window?.overrideUserInterfaceStyle = .unspecified
                    }else{
                        Model.sharedModel.settings.appearances = .light
                        switchDeviceSettings()?.setOn(false, animated: true)
                        (scene.delegate as? SceneDelegate)?.window?.overrideUserInterfaceStyle = .light
                    }
                case .light, .unspecified:
                    if sender.isOn{
                        Model.sharedModel.settings.appearances = .dark
                        (scene.delegate as? SceneDelegate)?.window?.overrideUserInterfaceStyle = .dark
                        switchDeviceSettings()?.setOn(false, animated: true)
                    }else{
                        (scene.delegate as? SceneDelegate)?.window?.overrideUserInterfaceStyle = .unspecified
                        Model.sharedModel.settings.appearances = .system
                    }
                default: return
                }
            }
        case 1:
            UIApplication.shared.connectedScenes.forEach { (scene: UIScene) in
                if sender.isOn{
                    (scene.delegate as? SceneDelegate)?.window?.overrideUserInterfaceStyle = .unspecified
                    Model.sharedModel.settings.appearances = .system
                    switch UIScreen.main.traitCollection.userInterfaceStyle{
                    case .dark: switchDarkMode()?.setOn(true, animated: true)
                    case .light, .unspecified: switchDarkMode()?.setOn(false, animated: true)
                    default: return
                    }
                }
            }
        default: return
        }
        saveChanges()
    }
    
    private func switchDarkMode() -> UISwitch?{
        guard let cell = tableView.cellForRow(at: IndexPath(item: 0, section: 0)) else {
            return nil
        }
        return (cell.accessoryView as? UISwitch)
    }
    
    private func switchDeviceSettings() -> UISwitch?{
        guard let cell = tableView.cellForRow(at: IndexPath(item: 1, section: 0)) else {
            return nil
        }
        return (cell.accessoryView as? UISwitch)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard switchDeviceSettings()?.isOn ?? false else {return}
        UIApplication.shared.connectedScenes.forEach { (scene: UIScene) in
            switch UIScreen.main.traitCollection.userInterfaceStyle {
            case .dark:
                (scene.delegate as? SceneDelegate)?.window?.overrideUserInterfaceStyle = .unspecified
                switchDarkMode()?.setOn(true, animated: true)
            case .light, .unspecified:
                (scene.delegate as? SceneDelegate)?.window?.overrideUserInterfaceStyle = .unspecified
                switchDarkMode()?.setOn(false, animated: true)
            default: return
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        saveChanges()
    }
    
    private func saveChanges(){
        let appearances = Model.sharedModel.settings
        UserDefaults.standard.set(try? PropertyListEncoder().encode(appearances), forKey:"appearances")
    }
}


extension AppTintViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        let switchOption = UISwitch(frame: .zero)
        switchOption.tag = indexPath.item
        switchOption.addTarget(self, action: #selector(valueChangeTint(sender:)), for: .valueChanged)
        switch indexPath.item {
        case 0:
            cell.textLabel?.text = "Dark mode"
            UIApplication.shared.connectedScenes.forEach { (scene: UIScene) in
                if Model.sharedModel.settings.appearances == .system{
                    switch UIScreen.main.traitCollection.userInterfaceStyle {
                    case .dark: switchOption.setOn(true, animated: false)
                    case .light, .unspecified: switchOption.setOn(false, animated: false)
                    default: break
                    }
                }else{
                    switch Model.sharedModel.settings.appearances {
                    case .dark: switchOption.setOn(true, animated: false)
                    case .light: switchOption.setOn(false, animated: false)
                    default: break
                    }
                }
            }
        case 1:
            cell.textLabel?.text = "User device settings"
            cell.detailTextLabel?.text = "Set Dark mode to use the light or Dark selection located in your device Distplay & Brightness settings."
            cell.detailTextLabel?.numberOfLines = 0
            cell.detailTextLabel?.textColor = .secondaryLabel
            switchOption.setOn(Model.sharedModel.settings.appearances == .system, animated: false)
        default: return cell
        }
        cell.accessoryView = switchOption
        cell.tintColor = .secondaryLabel
        cell.selectionStyle = .none
        return cell
    }
    

    
}
