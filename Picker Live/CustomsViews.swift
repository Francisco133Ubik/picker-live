//
//  CustomsViews.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 03/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit
import AVFoundation

class UICaptureButton: UIControl {
    private var animator = UIViewPropertyAnimator()
    private let normalColor = UIColor.tertiarySystemBackground
    private let highlightedColor = UIColor.systemGray4
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    private func sharedInit() {
        backgroundColor = normalColor
        addTarget(self, action: #selector(touchDown), for: [.touchDown, .touchDragEnter])
        addTarget(self, action: #selector(touchUp), for: [.touchUpInside, .touchDragExit, .touchCancel])
        
        add(UIImageView(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
            $0.image = UIImage(named: "icon_plus")
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: CGFloat(80).dp, height: CGFloat(80).dp)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.width / 2
    }
    
    @objc private func touchDown() {
        animator.stopAnimation(true)
        backgroundColor = highlightedColor
    }
    
    @objc private func touchUp() {
        animator = UIViewPropertyAnimator(duration: 0.5, curve: .easeOut, animations: {
            self.backgroundColor = self.normalColor
        })
        animator.startAnimation()
        feedback()
    }
    
    private func feedback(){
        guard Model.sharedModel.settings.isHapticsEnable else {
            return
        }
        let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .light)
        impactFeedbackgenerator.prepare()
        impactFeedbackgenerator.impactOccurred()
        AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(1117), nil)
    }
}


class ColorRangeView: UIView {
    public var color: UIColor = .red{didSet{
        updateView()
        }}
    
    lazy var colorLabel: UILabel = {
        let colorLabel = UILabel()
        colorLabel.textColor = .secondaryLabel
        colorLabel.textAlignment = .center
        colorLabel.text = "#FFFFFF"
        colorLabel.minimumScaleFactor = 0.1
        colorLabel.font = UIFont.systemFont(ofSize: CGFloat(16).dp, weight: .bold)
        colorLabel.adjustsFontSizeToFitWidth = true
        return colorLabel
    }()
    
    lazy var viewColor100: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = color
        view.clipsToBounds = true
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
        return view
    }()
    
    lazy var viewColor80: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = color.withAlphaComponent(0.8)
        return view
    }()
    
    lazy var viewColor60: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = color.withAlphaComponent(0.6)
        return view
    }()
    
    lazy var viewColor40: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = color.withAlphaComponent(0.4)
        view.clipsToBounds = true
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        return view
    }()
    
    func updateView(){
        viewColor100.backgroundColor = color
        viewColor80.backgroundColor = color.withAlphaComponent(0.8)
        viewColor60.backgroundColor = color.withAlphaComponent(0.6)
        viewColor40.backgroundColor = color.withAlphaComponent(0.4)
        colorLabel.text = color.toHexString().uppercased()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        backgroundColor = .tertiarySystemBackground
        clipsToBounds = true
        layer.masksToBounds = true

        addSubview(viewColor40)
        viewColor40.translatesAutoresizingMaskIntoConstraints = false
        viewColor40.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        viewColor40.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.75).isActive = true
        viewColor40.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15).isActive = true
        viewColor40.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -CGFloat(10).dp).isActive = true

        addSubview(viewColor60)
        viewColor60.translatesAutoresizingMaskIntoConstraints = false
        viewColor60.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        viewColor60.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.75).isActive = true
        viewColor60.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15).isActive = true
        viewColor60.trailingAnchor.constraint(equalTo: viewColor40.leadingAnchor).isActive = true
        
        addSubview(viewColor80)
        viewColor80.translatesAutoresizingMaskIntoConstraints = false
        viewColor80.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        viewColor80.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.75).isActive = true
        viewColor80.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15).isActive = true
        viewColor80.trailingAnchor.constraint(equalTo: viewColor60.leadingAnchor).isActive = true
    
        addSubview(viewColor100)
        viewColor100.translatesAutoresizingMaskIntoConstraints = false
        viewColor100.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        viewColor100.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.75).isActive = true
        viewColor100.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.15).isActive = true
        viewColor100.trailingAnchor.constraint(equalTo: viewColor80.leadingAnchor).isActive = true
        
        addSubview(colorLabel)
        colorLabel.translatesAutoresizingMaskIntoConstraints = false
        colorLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        colorLabel.trailingAnchor.constraint(equalTo: viewColor100.leadingAnchor, constant: -(bounds.size.width * 0.09)).isActive = true
        colorLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: bounds.size.width * 0.08).isActive = true
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
        viewColor40.layer.cornerRadius = viewColor40.frame.height / 2
        viewColor100.layer.cornerRadius = viewColor100.frame.height / 2
    }
}


class UICircleView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        clipsToBounds = true
        layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
}


class UIImageViewCircle: UIImageView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        clipsToBounds = true
        layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
}
