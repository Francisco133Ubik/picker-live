//
//  CollectionViewCells.swift
//  Picker Live
//
//  Created by Francisco Javier Delgado García on 09/07/20.
//  Copyright © 2020 Francisco Javier Delgado García. All rights reserved.
//

import UIKit

class RangeColorUICollectionViewCell: UICollectionViewCell {
    let rangeView = ColorRangeView(frame: .zero)
    
    var color: UIColor?{didSet{
        guard let color = color else {
            return
        }
        rangeView.color = color
        }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        add(rangeView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.heightAnchor.constraint(equalTo: heightAnchor),
                $0.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.9),
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: centerXAnchor)
            ])
        }
    }
}

class HeaderSectionCollectionReusableView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        add(label(color: .label,
                  font: UIFont.systemFont(ofSize: CGFloat(25).dp, weight: .bold))) {
            $0.text = "Captured Colors"
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: CGFloat(22).dp),
                $0.topAnchor.constraint(equalTo: topAnchor, constant: CGFloat(18).dp),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(44).dp)
            ])
        }
        add(label(color: .secondaryLabel,
                  font: UIFont.systemFont(ofSize: CGFloat(13).dp, weight: .medium))) {
            $0.text = "40%"
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -CGFloat(20).dp),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -CGFloat(30).dp),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(20).dp)
            ])
        }
        add(label(color: .secondaryLabel,
                  font: UIFont.systemFont(ofSize: CGFloat(13).dp, weight: .medium))) {
            $0.text = "60%"
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -CGFloat(20).dp),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -CGFloat(80).dp),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(20).dp)
            ])
        }
        add(label(color: .secondaryLabel,
                  font: UIFont.systemFont(ofSize: CGFloat(13).dp, weight: .medium))) {
            $0.text = "80%"
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -CGFloat(20).dp),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -CGFloat(135).dp),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(20).dp)
            ])
        }
        add(label(color: .secondaryLabel,
                  font: UIFont.systemFont(ofSize: CGFloat(13).dp, weight: .medium))) {
            $0.text = "100%"
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -CGFloat(20).dp),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -CGFloat(190).dp),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(20).dp)
            ])
        }
        add(label(color: .secondaryLabel,
                  font: UIFont.systemFont(ofSize: CGFloat(13).dp, weight: .medium))) {
            $0.text = "Color value"
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -CGFloat(20).dp),
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: CGFloat(22).dp),
                $0.heightAnchor.constraint(equalToConstant: CGFloat(20).dp)
            ])
        }
        add(UIView(frame: .zero)) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.backgroundColor = .tertiaryLabel
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: CGFloat(22).dp),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -CGFloat(35).dp),
                $0.heightAnchor.constraint(equalToConstant: 1),
                $0.topAnchor.constraint(equalTo: topAnchor, constant: CGFloat(75).dp)
            ])
        }
    }
    
    private func label(color: UIColor, font: UIFont) -> UILabel{
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = font
        label.textColor = color
        return label
    }
}


class ColorPickerCollectionViewCell: UICollectionViewCell {
    let viewBackgroundColor: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.black.withAlphaComponent(0.45).cgColor
        view.clipsToBounds = true
        return view
    }()
    
    let viewColor: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let colorLabel: UILabel = {
        let colorLabel = UILabel()
        colorLabel.translatesAutoresizingMaskIntoConstraints = false
        colorLabel.textColor = .white
        colorLabel.textAlignment = .left
        colorLabel.text = ""
        colorLabel.font = UIFont.systemFont(ofSize: CGFloat(12).dp, weight: .medium)
        return colorLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isSelected: Bool{didSet{
        if isSelected{
            UIView.animate(withDuration: 0.35) { self.colorLabel.alpha = 1 }
        }else{
            colorLabel.alpha =  0
        }
        }}
    
    private func setupView(){
        add(viewBackgroundColor) {
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.heightAnchor.constraint(equalTo: heightAnchor),
                $0.widthAnchor.constraint(equalTo: widthAnchor)
            ])
        }
        add(viewColor) {
            viewColor.dropShadow(shadowOffset: CGSize(width: 1, height: 0))
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.heightAnchor.constraint(equalTo: heightAnchor),
                $0.widthAnchor.constraint(equalTo: heightAnchor)
            ])
        }
        viewBackgroundColor.add(colorLabel) {
            $0.dropShadow()
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: CGFloat(43).dp)
            ])
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        viewBackgroundColor.alpha = 1
        viewColor.alpha = 1
    }
    
    var colorIndicator: UIColor?{
        didSet{
            guard let color = colorIndicator else { return }
            viewColor.backgroundColor = color
            viewBackgroundColor.backgroundColor = color.withAlphaComponent(0.85)
            colorLabel.text = color.toHexString().uppercased()
            colorLabel.alpha = 0
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        viewBackgroundColor.layer.cornerRadius = viewColor.frame.height / 2
        viewColor.layer.cornerRadius = viewBackgroundColor.frame.height / 2
    }
}
