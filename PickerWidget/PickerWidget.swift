//
//  PickerWidget.swift
//  PickerWidget
//
//  Created by Francisco on 17/04/23.
//  Copyright © 2023 Francisco Javier Delgado García. All rights reserved.
//

import WidgetKit
import SwiftUI

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), colors: DefaultColors)
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), colors: DefaultColors)
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate, colors: SimpleEntry.capturedColors())
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct PickerWidgetEntryView : View {
    @Environment(\.widgetFamily) var family
    var entry: Provider.Entry

    @ViewBuilder
    var body: some View {
        switch family {
        case .systemSmall:
            Text("Small")
        case .systemMedium:
            ZStack {
                Rectangle()
                    .foregroundColor(Color(.secondarySystemBackground))
                if entry.colors.isEmpty {
                    EmptyPlaceholderView()
                } else {
                    VStack(spacing: CGFloat(10).dp) {
                        TitleView()
                        ForEach(entry.colors.prefix(1), id: \.self) { colorItem in
                            ColorView(color: colorItem)
                                .frame(height: CGFloat(60).dp)
                        }
                    }
                    .padding(CGFloat(10).dp)
                }
            }
        case .systemLarge:
            ZStack {
                Rectangle()
                    .foregroundColor(Color(.secondarySystemBackground))
                if entry.colors.isEmpty {
                    EmptyPlaceholderView()
                } else {
                    VStack(spacing: CGFloat(10).dp) {
                        TitleView()
                        ForEach(entry.colors.prefix(4), id: \.self) { colorItem in
                            ColorView(color: colorItem)
                                .frame(height: CGFloat(60).dp)
                        }
                        if entry.colors.count < 4 {
                            Spacer()
                        }
                    }
                    .padding(CGFloat(10).dp)
                }
            }
        default:
            Text("Some other WidgetFamily in the future.")
        }
    }
}

@main
struct PickerWidget: Widget {
    let kind: String = "PickerWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            PickerWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("Captured Colors")
        .description("Get an overview of the last captured colors")
        .supportedFamilies([
                    .systemMedium,
                    .systemLarge,
                ])
    }
}

struct PickerWidget_Previews: PreviewProvider {
    static var previews: some View {
        PickerWidgetEntryView(entry: SimpleEntry(date: Date(),
                                                 colors: DefaultColors))
            .previewDevice(PreviewDevice(rawValue: "iPhone 14"))
            .previewDisplayName("iPhone 14")
            .previewContext(WidgetPreviewContext(family: .systemLarge))
        
        PickerWidgetEntryView(entry: SimpleEntry(date: Date(),
                                                 colors: DefaultColors))
            .previewDevice(PreviewDevice(rawValue: "iPhone 14 Pro Max"))
            .previewDisplayName("iPhone 14 Pro Max")
            .previewContext(WidgetPreviewContext(family: .systemLarge))
        
        PickerWidgetEntryView(entry: SimpleEntry(date: Date(),
                                                 colors: DefaultColors))
            .previewDevice(PreviewDevice(rawValue: "iPhone 8"))
            .previewDisplayName("iPhone 8")
            .previewContext(WidgetPreviewContext(family: .systemLarge))
    }
}

