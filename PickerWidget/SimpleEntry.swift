//
//  SimpleEntry.swift
//  PickerWidgetExtension
//
//  Created by Francisco on 17/04/23.
//  Copyright © 2023 Francisco Javier Delgado García. All rights reserved.
//
import WidgetKit
import SwiftUI

struct SimpleEntry: TimelineEntry {
    let date: Date
    let colors: [Color]
    
    static func capturedColors() -> [Color] {
        guard let data = UserDefaults(suiteName: SuiteName)?.object(forKey: ColorsKey) as? Data else {
            return [Color]()
        }
        do {
            guard let colors = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [UIColor]  else {
                return [Color]()
            }
            return colors.map { Color($0) }
        }catch {
            return [Color]()
        }
    }
}

let ColorsKey = "colors"
let SuiteName = "group.com.picker"
let DefaultColors = [Color(red: 0/255, green: 129/255, blue: 167/255),
                     Color(red: 0/255, green: 175/255, blue: 185/255),
                     Color(red: 254/255, green: 217/255, blue: 183/255),
                     Color(red: 240/255, green: 113/255, blue: 103/255)]



