//
//  TitleView.swift
//  Picker Live
//
//  Created by Francisco on 17/04/23.
//  Copyright © 2023 Francisco Javier Delgado García. All rights reserved.
//

import SwiftUI

struct TitleView: View {
    
    var body: some View {
        HStack(spacing: CGFloat(20).dp) {
            Text("  Value   ")
                .foregroundColor(Color(UIColor.secondaryLabel))
                .font(.system(size: CGFloat(14).dp, weight: .medium))
            Text("100%")
                .foregroundColor(Color(UIColor.secondaryLabel))
                .font(.system(size: CGFloat(14).dp, weight: .medium))
            Text("80%")
                .foregroundColor(Color(UIColor.secondaryLabel))
                .font(.system(size: CGFloat(14).dp, weight: .medium))
            Text("60%")
                .foregroundColor(Color(UIColor.secondaryLabel))
                .font(.system(size: CGFloat(14).dp, weight: .medium))
            Text("40%")
                .foregroundColor(Color(UIColor.secondaryLabel))
                .font(.system(size: CGFloat(14).dp, weight: .medium))
        }
        .padding(EdgeInsets(top: CGFloat(5).dp,
                            leading: CGFloat(5).dp,
                            bottom: CGFloat(5).dp,
                            trailing: CGFloat(10).dp))
    }
}
