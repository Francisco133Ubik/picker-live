//
//  EmptyView.swift
//  Picker Live
//
//  Created by Francisco on 18/04/23.
//  Copyright © 2023 Francisco Javier Delgado García. All rights reserved.
//

import SwiftUI

struct EmptyPlaceholderView: View {
    
    var body: some View {
        VStack {
            Image("imageIcon")
                .resizable()
                .frame(width: 100, height: 100)
            Text("Capture your first color!")
                .foregroundColor(Color(UIColor.label))
                .font(.system(size: CGFloat(14).dp, weight: .medium))
        }
    }
}


