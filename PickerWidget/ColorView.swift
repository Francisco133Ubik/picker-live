//
//  SwiftUIView.swift
//  PickerWidgetExtension
//
//  Created by Francisco on 17/04/23.
//  Copyright © 2023 Francisco Javier Delgado García. All rights reserved.
//

import SwiftUI

struct ColorView: View {
    let color: Color
    
    var body: some View {
        HStack {
            Text(UIColor(color).toHexString().uppercased())
                .font(.system(size: CGFloat(12).dp, weight: .medium))
                .padding(CGFloat(10).dp)
            HStack(spacing: 0) {
                Rectangle()
                    .fill(color)
                    .cornerRadius(50, corners: [.topLeft, .bottomLeft])
                Rectangle()
                    .fill(color)
                    .opacity(0.8)
                Rectangle()
                    .fill(color)
                    .opacity(0.6)
                Rectangle()
                    .fill(color)
                    .opacity(0.4)
                    .cornerRadius(50, corners: [.bottomRight, .topRight])
            }
        }
        .padding(CGFloat(8).dp)
        .background(
            Color(.tertiarySystemBackground)
                .clipShape(Capsule())
        )
    }
}
